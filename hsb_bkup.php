<?php
class hsb 
{
	const HSB_WSDL = "https://connect.healthaxis.com/ivr/IVR.asmx?WSDL";
	const RETARUS_WSDL = "https://faxws.us.retarus.com/Faxolution201203?wsdl";

	public function maskMemberId($memberId, $dnis) 
	{
		$memberIds = self::formatMemberIds($memberId, $dnis);
		if (is_array($memberIds)) {
			$member['memberIds'] = $memberIds;
			$result = array('message' => json_encode($member), 'status' => 200, 'type' => 'application/json');
			return $result;
		} else {
			$result = array('message' => "Member not found!", 'status' => 404, 'type' => 'test/plain');
			return $result;
		}
			
	}

	private function formatMemberIds($memberId, $group, $alphaChars)
	{
		//$db = new db();
                //$row = $db->getRow($dnis);
                //if (isset($row)) {
			$ids = array();
                        $alphaChars = explode('#', $alphaChars);
			//$groups = explode('#', $row['Group_ID']);
			//$member = array_combine($groups, $alphaChars);
			if (($group == "LAFRA") || ($group == "PALA")) {
				for ($i = 0; $i < count($alphaChars); $i++) {
					$ids[$i] = substr_replace($memberId, $alphaChars[$i], 3, 0);
                                }
			} else {
				for ($i = 0; $i < count($alphaChars); $i++) {
                                        $ids[$i] = $alphaChars[$i] . $memberId;
                                }
			}
                        return $ids;
                //} else {
                //        return FALSE;
                //}
	}

	public function getEligibility($partialId, $account, $groupId, $dob, $coverageType, $dnis, $alphaChars) 
	{
		$groups = explode('#', $groupId);
		foreach($groups AS $group) {
			$memberIds = self::formatMemberIds($partialId, $group, $alphaChars);
			__log("$group-" . json_encode($memberIds), $GLOBALS['uuid']);
			$result = array();
			if (is_array($memberIds)) {
				foreach ($memberIds AS $memberId) {
					try {
						$soap = new soap_client(self::HSB_WSDL);
						$data = array(
				        	        "Account" => $account,
	        	        			"GroupId" => $group,
					                "AltId" => $memberId,
	        	        			"DOB" => $dob
	        	        			);		
				                $soap->soapCall($coverageType, $data);
						$soapResult = $soap->result;
						__log(json_encode($soapResult), $GLOBALS['uuid']); 						
						if ($soap->status == "fault") {
							$result = array('message' => json_encode($soapResult), 'status' => 500, 'type' => 'application/json');
							break;
						} else {	
							if (isset($soapResult[$coverageType . "Result"]["Members"]["Eligibility"])) {
								if (isset($soapResult[$coverageType . "Result"]["Members"]["Eligibility"][0])) {
									$soapResult[$coverageType . "Result"]["MemberCount"] = count($soapResult[$coverageType . "Result"]["Members"]["Eligibility"]);
								} else {
									$soapResult[$coverageType . "Result"]["MemberCount"] = 1;
								}
                                                                $soapResult[$coverageType . "Result"]["MemberId"] = $memberId;
							} else {
								continue;
							}
                                                        if ($soapResult[$coverageType . "Result"]["MemberCount"] == 1) {
                                                                if ($soapResult[$coverageType . "Result"]["Members"]["Eligibility"]["StartDate"] != "") {
                                                                        $soapResult[$coverageType . "Result"]["Members"]["Eligibility"]["StartDate"] = date('m/d/Y', strtotime($soapResult[$coverageType . "Result"]["Members"]["Eligibility"]["StartDate"]));
                                                                }
                                                                 if ($soapResult[$coverageType . "Result"]["Members"]["Eligibility"]["PaidThroughDate"] != "") {
                                                                $soapResult[$coverageType . "Result"]["Members"]["Eligibility"]["PaidThroughDate"] = date('m/d/Y', strtotime($soapResult[$coverageType . "Result"]["Members"]["Eligibility"]["PaidThroughDate"]));
                                                                }
                                                                 if ($soapResult[$coverageType . "Result"]["Members"]["Eligibility"]["TerminationDate"] != "") {
                                                                $soapResult[$coverageType . "Result"]["Members"]["Eligibility"]["TerminationDate"] = date('m/d/Y', strtotime($soapResult[$coverageType . "Result"]["Members"]["Eligibility"]["TerminationDate"]));						
                                                                }
                                                        }
							$result = array('message' => json_encode($soapResult), 'status' => 200, 'type' => 'application/json');
							break;
						}
					} catch (exception $e) {
						$error = array('error' => $e->getMessage());
						$result = array('message' => json_encode($error), 'status' => 400, 'type' => 'application/json');
					}
				}
			}
			if (!empty($result)) {
				break;
			}
		}
		if (empty($result)) {
			$result = array('message' => "Member not found!", 'status' => 404, 'type' => 'text/plain');
		}
		return $result;		
	}

	/*public function getClaimInfo($memberId, $account, $groupId, $dob, $taxId, $incurDate, $dnis, $claimNo = 0)
	{
		//$memberIds = self::formatMemberIds($memberId, $dnis);
		$memberIds = array($groupId => $memberId);
                $result = array();
                if (is_array($memberIds)) {
                        foreach ($memberIds as $group => $memberId) {
                                try {
                                        $soap = new soap_client(self::HSB_WSDL);
                                        $data = array(
                                                "Account" => $account,
                                                "GroupId" => $group,
                                                "AltId" => $memberId,
                                                "DOB" => $dob,
                                                "TaxId" => $taxId,
                                                "ServiceDate" => $incurDate
                                                );

                                        $soap->soapCall('ClaimLookup', $data);
                                        $soapResult = $soap->result;
                                        if ($soap->status == "fault") {
                                                $result = array('message' => json_encode($soapResult), 'status' => 500, 'type' => 'application/json');
                                                break;
                                        } else {
                                                if (is_array($soapResult["ClaimLookupResult"]["ClaimDependent"])) {
							$dependentCount = count($soapResult["ClaimLookupResult"]["ClaimDependent"]);
                                                        $soapResult["ClaimLookupResult"]["ClaimDependent"]["DependentCount"] = $dependentCount;						
							if ($dependentCount == 1) {
								if (is_array($soapResult["ClaimLookupResult"]["ClaimDependent"]["ClaimDependent"]["Claims"]["Claim"])) {
                                                                	$soapResult["ClaimLookupResult"]["ClaimDependent"]["ClaimDependent"]["Claims"]["ClaimCount"] = count($soapResult["ClaimLookupResult"]["ClaimDependent"]["ClaimDependent"]["Claims"]["Claim"]);
                                                                } else {
                                                                        $soapResult["ClaimLookupResult"]["ClaimDependent"]["ClaimDependent"]["Claims"]["ClaimCount"] = 0;
                                                                }
							} else {
								for ($i = 0; $i < $dependentCount; $i++) {
									if (is_array($soapResult["ClaimLookupResult"]["ClaimDependent"][$i]["ClaimDependent"]["Claims"]["Claim"])) {
										$soapResult["ClaimLookupResult"]["ClaimDependent"][$i]["ClaimDependent"]["Claims"]["ClaimCount"] = count($soapResult["ClaimLookupResult"]["ClaimDependent"][$i]["ClaimDependent"]["Claims"]["Claim"]);
									} else {
										$soapResult["ClaimLookupResult"]["ClaimDependent"][$i]["ClaimDependent"]["Claims"]["ClaimCount"] = 0;
									}
								}
							}
                                                } else {
                                                        continue;
                                                }
                                                //$result = array('message' => json_encode($soapResult["ClaimLookupResult"]), 'status' => 200, 'type' => 'application/json');
						$result = array('message' => json_encode($soapResult), 'status' => 200, 'type' => 'application/json');
						break;
                                        }
                                } catch (exception $e) {
                                        $error = array('error' => $e->getMessage());
                                        $result = array('message' => json_encode($error), 'status' => 400, 'type' => 'application/json');
                                }
                        }
                }
                if (empty($result)) {
                        $result = array('message' => "Member not found!", 'status' => 404, 'type' => 'text/plain');
                }
                return $result;
	}*/

	public function getClaimInfo($partialId, $account, $groupId, $dob, $taxId, $incurDate, $dnis, $claimNo = 0, $alphaChars)
        {
	        $groups = explode('#', $groupId);
		foreach ($groups AS $group) {       
			$memberIds = self::formatMemberIds($partialId, $group, $alphaChars);
			__log("$group-" . json_encode($memberIds), $GLOBALS['uuid']);
	                $result = array();
	                if (is_array($memberIds)) {
	                        foreach ($memberIds AS $memberId) {
	                                try {
	                                        $soap = new soap_client(self::HSB_WSDL);
	                                        $data = array(
	                                                "Account" => $account,
	                                                "GroupId" => $group,
	                                                "AltId" => $memberId,
	                                                "DOB" => $dob,
	                                                "TaxId" => $taxId,
	                                                "ServiceDate" => $incurDate
	                                                );
	
	                                        $soap->soapCall('ClaimLookup', $data);
	                                        $soapResult = $soap->result;
	                                        __log(json_encode($soapResult), $GLOBALS['uuid']);
	                                        if ($soap->status == "fault") {
	                                                $result = array('message' => json_encode($soapResult), 'status' => 500, 'type' => 'application/json');
	                                                break;
	                                        } else {
	                                                if (isset($soapResult["ClaimLookupResult"]["ClaimDependent"]["ClaimDependent"])) {
								if (isset($soapResult["ClaimLookupResult"]["ClaimDependent"]["ClaimDependent"][0])) {
		                                                        $dependentCount = count($soapResult["ClaimLookupResult"]["ClaimDependent"]["ClaimDependent"]);
		                                                        $soapResult["ClaimLookupResult"]["ClaimDependent"]["ClaimDependent"]["DependentCount"] = $dependentCount;
								} else {
									$dependentCount = 1;
                                                                        $soapResult["ClaimLookupResult"]["ClaimDependent"]["ClaimDependent"]["DependentCount"] = $dependentCount;
								}
	                                                        if ($dependentCount == 1) {
	                                                                if (isset($soapResult["ClaimLookupResult"]["ClaimDependent"]["ClaimDependent"]["Claims"]["Claim"])) {
										if (isset($soapResult["ClaimLookupResult"]["ClaimDependent"]["ClaimDependent"]["Claims"]["Claim"][0])) {
		                                                                        $soapResult["ClaimLookupResult"]["ClaimDependent"]["ClaimDependent"]["Claims"]["ClaimCount"] = count($soapResult["ClaimLookupResult"]["ClaimDependent"]["ClaimDependent"]["Claims"]["Claim"]);
											$soapResult["ClaimLookupResult"]["ClaimDependent"]["ClaimDependent"]["Claims"]["Claim"] = $soapResult["ClaimLookupResult"]["ClaimDependent"]["ClaimDependent"]["Claims"]["Claim"][$claimNo];
										} else {
											$soapResult["ClaimLookupResult"]["ClaimDependent"]["ClaimDependent"]["Claims"]["ClaimCount"] = 1;
										}
	                                                                } else {
	                                                                        $soapResult["ClaimLookupResult"]["ClaimDependent"]["ClaimDependent"]["Claims"]["ClaimCount"] = 0;
	                                                                }
	                                                        }// else {
	                                                        /*        for ($i = 0; $i < $dependentCount; $i++) {
	                                                                        if (is_array($soapResult["ClaimLookupResult"]["ClaimDependent"]["ClaimDependent"][$i]["Claims"]["Claim"])) {
											if (isset($soapResult["ClaimLookupResult"]["ClaimDependent"]["ClaimDependent"]["Claims"]["Claim"][0])) {
		                                                                                $soapResult["ClaimLookupResult"]["ClaimDependent"][$i]["ClaimDependent"]["Claims"]["ClaimCount"] = count($soapResult["ClaimLookupResult"]["ClaimDependent"][$i]["ClaimDependent"]["Claims"]["Claim"]);
											} else {
												$soapResult["ClaimLookupResult"]["ClaimDependent"][$i]["ClaimDependent"]["Claims"]["ClaimCount"] = 1;
											}
	                                                                        } else {
	                                                                                $soapResult["ClaimLookupResult"]["ClaimDependent"][$i]["ClaimDependent"]["Claims"]["ClaimCount"] = 0;
	                                                                        }
	                                                                }
	                                                        }*/
                                                                $soapResult["ClaimLookupResult"]["MemberId"] = $memberId;
	                                                } else {
	                                                        continue;
	                                                }
	                                                //$result = array('message' => json_encode($soapResult["ClaimLookupResult"]), 'status' => 200, 'type' => 'application/json');
							if (isset($soapResult["ClaimLookupResult"]["ClaimDependent"]["ClaimDependent"]["Claims"]["Claim"]) && $soapResult["ClaimLookupResult"]["ClaimDependent"]["ClaimDependent"]["Claims"]["Claim"]["ClaimDate"] != "") {
                                                                $soapResult["ClaimLookupResult"]["ClaimDependent"]["ClaimDependent"]["Claims"]["Claim"]["ClaimDate"] = date('m/d/Y', strtotime($soapResult["ClaimLookupResult"]["ClaimDependent"]["ClaimDependent"]["Claims"]["Claim"]["ClaimDate"]));
                                                        }
	                                                $result = array('message' => json_encode($soapResult), 'status' => 200, 'type' => 'application/json');
	                                                break;
	                                        }
	                                } catch (exception $e) {
	                                        $error = array('error' => $e->getMessage());
                	                        $result = array('message' => json_encode($error), 'status' => 400, 'type' => 'application/json');
	  	                   	}
                        	}
			}
			if (!empty($result)) {
                                break;
                        }
		}
		if (empty($result)) {
                        $result = array('message' => "Member not found!", 'status' => 404, 'type' => 'text/plain');
                }
//error_log("\n\r" . implode($result), 3, "./errors.log");
                return $result;
        }

	public function benefitsFax($member, $account, $group, $tax, $dob, $coverage, $summaryDoc, $faxNo)
	{
		try {
			$fax = new soap_client(self::RETARUS_WSDL);
			//$doc = file_get_contents('https://www.hsbdirectoriesonline.com/bensum/plandocs/' . '.pdf');
			//$doc64 = base64_encode($doc);
			$soap = new soap_client(self::HSB_WSDL);
                        $data = array(
                                      "Account" => $account,
                                      "GroupId" => $group,
                                      "AltId" => $member,
                                      "DOB" => $dob
                                     );
                        $soap->soapCall($coverage, $data);
                        $soapResult = $soap->result;
                        if ($soap->status == "fault") {
	                        $result = array('message' => json_encode($soapResult), 'status' => 500, 'type' => 'application/json');
                                return $result;
                        } else {
                                if ($soapResult[$coverage . "Result"]["Members"]["Eligibility"]["StartDate"] != "") {
		                        $soapResult[$coverage . "Result"]["Members"]["Eligibility"]["StartDate"] = date('m/d/Y', strtotime($soapResult[$coverage . "Result"]["Members"]["Eligibility"]["StartDate"]));
	                        }	                        
	                        if ($soapResult[$coverage . "Result"]["Members"]["Eligibility"]["TerminationDate"] != "") {
	                   	     $soapResult[$coverage . "Result"]["Members"]["Eligibility"]["TerminationDate"] = date('m/d/Y', strtotime($soapResult[$coverage . "Result"]["Members"]["Eligibility"]["TerminationDate"]));
	                        }
                        }
			$property = array();
			$property[] = array('key' => 'AltId', 'value' => $member);
			$property[] = array('key' => 'ToFaxNum', 'value' => $faxNo);
			$property[] = array('key' => 'TaxId', 'value' => $tax);
			$property[] = array('key' => 'CurrentDate', 'value' => date("m/d/Y"));
			$property[] = array('key' => 'FirstName', 'value' => $soapResult[$coverage . "Result"]["Members"]["Eligibility"]['FirstName']);
			$property[] = array('key' => 'LastName', 'value' => $soapResult[$coverage . "Result"]["Members"]["Eligibility"]['LastName']);
                        $property[] = array('key' => 'DOB', 'value' => $soapResult[$coverage . "Result"]["Members"]["Eligibility"]['DOB']);
			$property[] = array('key' => 'GroupId', 'value' => $group);
			$property[] = array('key' => 'StartDate', 'value' => $soapResult[$coverage . "Result"]["Members"]["Eligibility"]['StartDate']);
			$property[] = array('key' => 'EndDate', 'value' => $soapResult[$coverage . "Result"]["Members"]["Eligibility"]['TerminationDate']);
			if ($summaryDoc == 1) {
				$doc = file_get_contents('https://www.hsbdirectoriesonline.com/bensum/plandocs/' . $soapResult[$coverage . "Result"]["Members"]["Eligibility"]['TI3Code'] . '.pdf');
				//$doc = file_get_contents('https://www.hsbdirectoriesonline.com/bensum/plandocs/CBUSAMED61.pdf');
	                        $doc64 = base64_encode($doc);
				$faxdata = array(
							'JobRequest' => array(
				        		'username' => 'inference-hsb-ivr@inferencesolutions.com',
			        		        'password' => '!Password02',
			       		        	'faxRecipient' => array('number' => $faxNo, 'property' => $property),
							//'faxRecipient' => array('number' => $faxNo),
					               	'document' => array('name' => 'doc00.pdf', 'data' => $doc64),
							//'options' => array('resolution' => "HIGH")
							'options' => array('resolution' => "HIGH", 'coverpageTemplateName' => 'HSBBenefitsFaxback.html')
					        	)
						);
			} else {
				$faxdata = array(
                                                        'JobRequest' => array(
                                                        'username' => 'inference-hsb-ivr@inferencesolutions.com',
                                                        'password' => '!Password02',
                                                        'faxRecipient' => array('number' => $faxNo, 'property' => $property),
                                                        //'faxRecipient' => array('number' => $faxNo),
                                                        'document' => array('name' => 'doc00.html'),
                                                        //'options' => array('resolution' => "HIGH")
                                                        'options' => array('resolution' => "HIGH", 'coverpageTemplateName' => 'HSBBenefitsFaxback.html')
                                                        )
                                                );
			}
//var_dump($faxdata);exit;
			$fax->soapCall('sendFaxJob', $faxdata);
        	        $soapResult = $fax->result;
			if ($fax->status == "fault") {
                	        $result = array('message' => json_encode($soapResult), 'status' => 500, 'type' => 'application/json');                        
                        } else {
	                        $result = array('message' => json_encode($soapResult), 'status' => 200, 'type' => 'application/json');
                        }			
		} catch (exception $e) {
			$error = array('error' => $e->getMessage());
                        $result = array('message' => json_encode($error), 'status' => 400, 'type' => 'application/json');
		}
		return $result;			
	}

	public function claimsFax($member, $account, $group, $dob, $taxId, $incurDate, $faxNo)
	{
		try {
                        $fax = new soap_client(self::RETARUS_WSDL);
                        //$doc = file_get_contents('https://www.hsbdirectoriesonline.com/bensum/plandocs/' . '.pdf');
                        //$doc64 = base64_encode($doc);
                        $soap = new soap_client(self::HSB_WSDL);
                        $data = array(
                                      "Account" => $account,
                                      "GroupId" => $group,
                                      "AltId" => $member,
                                      "DOB" => ""
                                     );
                        $coverage = "MedicalLookup";
                        $soap->soapCall($coverage, $data);
                        $soapResult = $soap->result;
                        if ($soap->status == "fault") {
	                        $result = array('message' => json_encode($soapResult), 'status' => 500, 'type' => 'application/json');
                                return $result;
                        } else {                                  
                                $property = array();
                                $property[] = array('key' => 'AltId', 'value' => $member);
                                $property[] = array('key' => 'ToFaxNum', 'value' => $faxNo);
                                $property[] = array('key' => 'TaxId', 'value' => $taxId);
                                $property[] = array('key' => 'CurrentDate', 'value' => date("m/d/Y"));
                                $property[] = array('key' => 'SubscriberFirstName', 'value' => $soapResult[$coverage . "Result"]["Members"]["Eligibility"]['FirstName']);
                                $property[] = array('key' => 'SubscriberLastName', 'value' => $soapResult[$coverage . "Result"]["Members"]["Eligibility"]['LastName']);
                                $property[] = array('key' => 'GroupId', 'value' => $group);

                                if ($dob != '') {
                                        $data = array(
                                                      "Account" => $account,
                                                      "GroupId" => $group,
                                                      "AltId" => $member, 
                                                      "DOB" => $dob
                                                     );
                                        $soap->soapCall($coverage, $data);
                                        $soapResult = $soap->result;
                                        if ($soap->status == "fault") {
                                                $result = array('message' => json_encode($soapResult), 'status' => 500, 'type' => 'application/json');
                                                return $result;
                                        } else {
                                                if ($soapResult[$coverage . "Result"]["Members"]["Eligibility"]["StartDate"] != "") {
                                                        $soapResult[$coverage . "Result"]["Members"]["Eligibility"]["StartDate"] = date('m/d/Y', strtotime($soapResult[$coverage . "Result"]["Members"]["Eligibility"]["StartDate"]));
                                                }                                       
                                                if ($soapResult[$coverage . "Result"]["Members"]["Eligibility"]["TerminationDate"] != "") {
                                                     $soapResult[$coverage . "Result"]["Members"]["Eligibility"]["TerminationDate"] = date('m/d/Y', strtotime($soapResult[$coverage . "Result"]["Members"]["Eligibility"]["TerminationDate"]));
                                                }
                                        }
                                        $property[] = array('key' => 'FirstName', 'value' => $soapResult[$coverage . "Result"]["Members"]["Eligibility"]['FirstName']);
                                        $property[] = array('key' => 'LastName', 'value' => $soapResult[$coverage . "Result"]["Members"]["Eligibility"]['LastName']);  
                                        $property[] = array('key' => 'DOB', 'value' => $soapResult[$coverage . "Result"]["Members"]["Eligibility"]['DOB']);
                                        $property[] = array('key' => 'StartDate', 'value' => $soapResult[$coverage . "Result"]["Members"]["Eligibility"]['StartDate']);
                                        $property[] = array('key' => 'EndDate', 'value' => $soapResult[$coverage . "Result"]["Members"]["Eligibility"]['TerminationDate']);
                                }

                                $data = array(
                                              "Account" => $account,
                                              "GroupId" => $group,
                                              "AltId" => $member,
                                              "DOB" => $dob,
                                              //"TaxId" => $taxId,
                                              "ServiceDate" => $incurDate				      
                                             );
                                $soap->soapCall("ClaimLookup", $data);
                                $soapResult = $soap->result;
                                if ($soap->status == "fault") {
                                        $result = array('message' => json_encode($soapResult), 'status' => 500, 'type' => 'application/json');
                                        return $result;
                                } else {                                  
                                        if (isset($soapResult["ClaimLookupResult"]["ClaimDependent"]["ClaimDependent"]["Claims"]["Claim"][0])) {
                                                $count = 0;
                                                foreach ($soapResult["ClaimLookupResult"]["ClaimDependent"]["ClaimDependent"]["Claims"]["Claim"] AS $claim) {
                                                        if (isset($soapResult["ClaimLookupResult"]["ClaimDependent"]["ClaimDependent"]["Claims"]["Claim"][$count]["TaxId"])) {
                                                                $property[] = array('key' => 'TaxID' . ($count + 1), 'value' => $soapResult["ClaimLookupResult"]["ClaimDependent"]["ClaimDependent"]["Claims"]["Claim"][$count]["TaxId"]);
                                                        }
                                                        if (isset($soapResult["ClaimLookupResult"]["ClaimDependent"]["ClaimDependent"]["Claims"]["Claim"][$count]["ClaimNumber"])) {
                                                                $property[] = array('key' => 'ClaimNumber' . ($count + 1), 'value' => $soapResult["ClaimLookupResult"]["ClaimDependent"]["ClaimDependent"]["Claims"]["Claim"][$count]["ClaimNumber"]);
                                                        }
                                                        if (isset($soapResult["ClaimLookupResult"]["ClaimDependent"]["ClaimDependent"]["Claims"]["Claim"][$count]["ServiceDate"])) {
                                                                $property[] = array('key' => 'ServiceDate' . ($count + 1), 'value' => $soapResult["ClaimLookupResult"]["ClaimDependent"]["ClaimDependent"]["Claims"]["Claim"][$count]["ServiceDate"]);
                                                        }
                                                        if (isset($soapResult["ClaimLookupResult"]["ClaimDependent"]["ClaimDependent"]["Claims"]["Claim"][$count]["CurrentChargeAmount"])) {
                                                                $property[] = array('key' => 'CurrentChargeAmount' . ($count + 1), 'value' => $soapResult["ClaimLookupResult"]["ClaimDependent"]["ClaimDependent"]["Claims"]["Claim"][$count]["CurrentChargeAmount"]);
                                                        }
                                                        if (isset($soapResult["ClaimLookupResult"]["ClaimDependent"]["ClaimDependent"]["Claims"]["Claim"][$count]["ApprovedAmount"])) {
                                                                $property[] = array('key' => 'ApprovedAmount' . ($count + 1), 'value' => $soapResult["ClaimLookupResult"]["ClaimDependent"]["ClaimDependent"]["Claims"]["Claim"][$count]["ApprovedAmount"]);
                                                        }
                                                        if (isset($soapResult["ClaimLookupResult"]["ClaimDependent"]["ClaimDependent"]["Claims"]["Claim"][$count]["PaidAmount"])) {
                                                                $property[] = array('key' => 'PaidAmount' . ($count + 1), 'value' => $soapResult["ClaimLookupResult"]["ClaimDependent"]["ClaimDependent"]["Claims"]["Claim"][$count]["PaidAmount"]);
                                                        }
                                                        if (isset($soapResult["ClaimLookupResult"]["ClaimDependent"]["ClaimDependent"]["Claims"]["Claim"][$count]["PatientLiability"])) {
                                                                $property[] = array('key' => 'PatientLiability' . ($count + 1), 'value' => $soapResult["ClaimLookupResult"]["ClaimDependent"]["ClaimDependent"]["Claims"]["Claim"][$count]["PatientLiability"]);
                                                        }
                                                        if (isset($soapResult["ClaimLookupResult"]["ClaimDependent"]["ClaimDependent"]["Claims"]["Claim"][$count]["PaidDate"])) {
                                                                $property[] = array('key' => 'PaidDate' . ($count + 1), 'value' => $soapResult["ClaimLookupResult"]["ClaimDependent"]["ClaimDependent"]["Claims"]["Claim"][$count]["PaidDate"]);
                                                        }
                                                        $count++;
                                                        if ($count > 4) {
                                                                break;
                                                        }
                                                }
                                        } else {
                                                if (isset($soapResult["ClaimLookupResult"]["ClaimDependent"]["ClaimDependent"]["Claims"]["Claim"]["TaxId"])) {
                                                        $property[] = array('key' => 'TaxID1', 'value' => $soapResult["ClaimLookupResult"]["ClaimDependent"]["ClaimDependent"]["Claims"]["Claim"]["TaxId"]);
                                                }
                                                if (isset($soapResult["ClaimLookupResult"]["ClaimDependent"]["ClaimDependent"]["Claims"]["Claim"]["ClaimNumber"])) {
                                                        $property[] = array('key' => 'ClaimNumber1', 'value' => $soapResult["ClaimLookupResult"]["ClaimDependent"]["ClaimDependent"]["Claims"]["Claim"]["ClaimNumber"]);
                                                }
                                                if (isset($soapResult["ClaimLookupResult"]["ClaimDependent"]["ClaimDependent"]["Claims"]["Claim"]["ServiceDate"])) {
                                                        $property[] = array('key' => 'ServiceDate1', 'value' => $soapResult["ClaimLookupResult"]["ClaimDependent"]["ClaimDependent"]["Claims"]["Claim"]["ServiceDate"]);
                                                }
                                                if (isset($soapResult["ClaimLookupResult"]["ClaimDependent"]["ClaimDependent"]["Claims"]["Claim"]["CurrentChargeAmount"])) {
                                                        $property[] = array('key' => 'CurrentChargeAmount1', 'value' => $soapResult["ClaimLookupResult"]["ClaimDependent"]["ClaimDependent"]["Claims"]["Claim"]["CurrentChargeAmount"]);
                                                }
                                                if (isset($soapResult["ClaimLookupResult"]["ClaimDependent"]["ClaimDependent"]["Claims"]["Claim"]["ApprovedAmount"])) {
                                                        $property[] = array('key' => 'ApprovedAmount1', 'value' => $soapResult["ClaimLookupResult"]["ClaimDependent"]["ClaimDependent"]["Claims"]["Claim"]["ApprovedAmount"]);
                                                }
                                                if (isset($soapResult["ClaimLookupResult"]["ClaimDependent"]["ClaimDependent"]["Claims"]["Claim"]["PaidAmount"])) {
                                                        $property[] = array('key' => 'PaidAmount1', 'value' => $soapResult["ClaimLookupResult"]["ClaimDependent"]["ClaimDependent"]["Claims"]["Claim"]["PaidAmount"]);
                                                }
                                                if (isset($soapResult["ClaimLookupResult"]["ClaimDependent"]["ClaimDependent"]["Claims"]["Claim"]["PatientLiability"])) {
                                                        $property[] = array('key' => 'PatientLiability1', 'value' => $soapResult["ClaimLookupResult"]["ClaimDependent"]["ClaimDependent"]["Claims"]["Claim"]["PatientLiability"]);
                                                }
                                                if (isset($soapResult["ClaimLookupResult"]["ClaimDependent"]["ClaimDependent"]["Claims"]["Claim"]["PaidDate"])) {
                                                        $property[] = array('key' => 'PaidDate1', 'value' => $soapResult["ClaimLookupResult"]["ClaimDependent"]["ClaimDependent"]["Claims"]["Claim"]["PaidDate"]);
                                                }
                                        }
                                }

                                $faxdata = array(
                                        'JobRequest' => array(
                                                        'username' => 'inference-hsb-ivr@inferencesolutions.com',
                                                        'password' => '!Password02',
                                                        'faxRecipient' => array('number' => $faxNo, 'property' => $property),
                                                        //'faxRecipient' => array('number' => $faxNo),
                                                        'document' => array('name' => 'doc00.html'),
                                                        //'options' => array('resolution' => "HIGH")
                                                        'options' => array('resolution' => "HIGH", 'coverpageTemplateName' => 'HSBClaimsFaxback.html')
                                                        )
                                        );
        //var_dump($faxdata);exit;
                                $fax->soapCall('sendFaxJob', $faxdata);
                                $soapResult = $fax->result;
                                if ($fax->status == "fault") {
                                        $result = array('message' => json_encode($soapResult), 'status' => 500, 'type' => 'application/json');
                                } else {
                                        $result = array('message' => json_encode($soapResult), 'status' => 200, 'type' => 'application/json');
                                }
                        }
                } catch (exception $e) {
                        $error = array('error' => $e->getMessage());
                        $result = array('message' => json_encode($error), 'status' => 400, 'type' => 'application/json');
                }
                return $result;
	}
        
        public function eligibilityFax($member, $account, $group, $tax, $dob, $coverage, $faxNo) 
        {
                try {
                        $fax = new soap_client(self::RETARUS_WSDL);
                        //$doc = file_get_contents('https://www.hsbdirectoriesonline.com/bensum/plandocs/' . '.pdf');
                        //$doc64 = base64_encode($doc);
                        $soap = new soap_client(self::HSB_WSDL);
                        $data = array(
                                      "Account" => $account,
                                      "GroupId" => $group,
                                      "AltId" => $member,
                                      "DOB" => ""
                                     );
                        $coverage = "MedicalLookup";
                        $soap->soapCall($coverage, $data);
                        $soapResult = $soap->result;
                        if ($soap->status == "fault") {
	                        $result = array('message' => json_encode($soapResult), 'status' => 500, 'type' => 'application/json');
                                return $result;
                        } else {                                  
                                $property = array();
                                $property[] = array('key' => 'AltId', 'value' => $member);
                                $property[] = array('key' => 'ToFaxNum', 'value' => $faxNo);
                                $property[] = array('key' => 'TaxId', 'value' => $tax);
                                $property[] = array('key' => 'CurrentDate', 'value' => date("m/d/Y"));
                                $property[] = array('key' => 'SubscriberFirstName', 'value' => $soapResult[$coverage . "Result"]["Members"]["Eligibility"]['FirstName']);
                                $property[] = array('key' => 'SubscriberLastName', 'value' => $soapResult[$coverage . "Result"]["Members"]["Eligibility"]['LastName']);
                                $property[] = array('key' => 'GroupId', 'value' => $group);

                                if ($dob != '') {
                                        $data = array(
                                                      "Account" => $account,
                                                      "GroupId" => $group,
                                                      "AltId" => $member, 
                                                      "DOB" => $dob
                                                     );
                                        $soap->soapCall($coverage, $data);
                                        $soapResult = $soap->result;
                                        if ($soap->status == "fault") {
                                                $result = array('message' => json_encode($soapResult), 'status' => 500, 'type' => 'application/json');
                                                return $result;
                                        } else {
                                                /*if ($soapResult[$coverage . "Result"]["Members"]["Eligibility"]["StartDate"] != "") {
                                                        $soapResult[$coverage . "Result"]["Members"]["Eligibility"]["StartDate"] = date('m/d/Y', strtotime($soapResult[$coverage . "Result"]["Members"]["Eligibility"]["StartDate"]));
                                                }                                       
                                                if ($soapResult[$coverage . "Result"]["Members"]["Eligibility"]["TerminationDate"] != "") {
                                                     $soapResult[$coverage . "Result"]["Members"]["Eligibility"]["TerminationDate"] = date('m/d/Y', strtotime($soapResult[$coverage . "Result"]["Members"]["Eligibility"]["TerminationDate"]));
                                                }*/
                                                if (getArrayKey('StartDate', getArrayKey('Eligibility', getArrayKey('Members', getArrayKey($coverage . 'Result', $soapResult)))) != "") {
                                                        $soapResult[$coverage . "Result"]["Members"]["Eligibility"]["StartDate"] = date('m/d/Y', strtotime($soapResult[$coverage . "Result"]["Members"]["Eligibility"]["StartDate"]));
                                                }                                       
                                                if (getArrayKey('TerminationDate', getArrayKey('Eligibility', getArrayKey('Members', getArrayKey($coverage . 'Result', $soapResult)))) != "") {
                                                     $soapResult[$coverage . "Result"]["Members"]["Eligibility"]["TerminationDate"] = date('m/d/Y', strtotime($soapResult[$coverage . "Result"]["Members"]["Eligibility"]["TerminationDate"]));
                                                }
                                        }
                                        /*$property[] = array('key' => 'FirstName', 'value' => $soapResult[$coverage . "Result"]["Members"]["Eligibility"]['FirstName']);
                                        $property[] = array('key' => 'LastName', 'value' => $soapResult[$coverage . "Result"]["Members"]["Eligibility"]['LastName']);  
                                        $property[] = array('key' => 'DOB', 'value' => $soapResult[$coverage . "Result"]["Members"]["Eligibility"]['DOB']);
                                        $property[] = array('key' => 'StartDate', 'value' => $soapResult[$coverage . "Result"]["Members"]["Eligibility"]['StartDate']);
                                        $property[] = array('key' => 'EndDate', 'value' => $soapResult[$coverage . "Result"]["Members"]["Eligibility"]['TerminationDate']);*/
                                        $property[] = array('key' => 'FirstName', 'value' => getArrayKey('FirstName', getArrayKey('Eligibility', getArrayKey('Members', getArrayKey($coverage . 'Result', $soapResult)))));
                                        $property[] = array('key' => 'LastName', 'value' => getArrayKey('LastName', getArrayKey('Eligibility', getArrayKey('Members', getArrayKey($coverage . 'Result', $soapResult)))));  
                                        $property[] = array('key' => 'DOB', 'value' => getArrayKey('DOB', getArrayKey('Eligibility', getArrayKey('Members', getArrayKey($coverage . 'Result', $soapResult)))));
                                        $property[] = array('key' => 'StartDate', 'value' => getArrayKey('StartDate', getArrayKey('Eligibility', getArrayKey('Members', getArrayKey($coverage . 'Result', $soapResult)))));
                                        $property[] = array('key' => 'EndDate', 'value' => getArrayKey('TerminationDate', getArrayKey('Eligibility', getArrayKey('Members', getArrayKey($coverage . 'Result', $soapResult)))));
                                }

                                $faxdata = array(
                                        'JobRequest' => array(
                                                        'username' => 'inference-hsb-ivr@inferencesolutions.com',
                                                        'password' => '!Password02',
                                                        'faxRecipient' => array('number' => $faxNo, 'property' => $property),
                                                        //'faxRecipient' => array('number' => $faxNo),
                                                        'document' => array('name' => 'doc00.html'),
                                                        //'options' => array('resolution' => "HIGH")
                                                        'options' => array('resolution' => "HIGH", 'coverpageTemplateName' => 'HSBEligibilityFaxback.html')
                                                        )
                                        );
        //var_dump($faxdata);exit;
                                $fax->soapCall('sendFaxJob', $faxdata);
                                $soapResult = $fax->result;
                                if ($fax->status == "fault") {
                                        $result = array('message' => json_encode($soapResult), 'status' => 500, 'type' => 'application/json');
                                } else {
                                        $result = array('message' => json_encode($soapResult), 'status' => 200, 'type' => 'application/json');
                                }
                        }
                } catch (exception $e) {
                        $error = array('error' => $e->getMessage());
                        $result = array('message' => json_encode($error), 'status' => 400, 'type' => 'application/json');
                }
                return $result;
        }
}