<?php
define("DEBUG", true);

function __log($data, $uuid = '') 
{
	if (DEBUG == true) {
		if (is_array($data)) {
        		error_log("\n" . date("Y-m-d H:i:s") . " -- UUID: $uuid" .  " -- Result: " . implode(", ", $data) . "\n", 3, "/var/log/hsb.log");
		} else {
			error_log("\n" . date("Y-m-d H:i:s") . " -- UUID: $uuid -- Result: $data" . "\n", 3, "/var/log/hsb.log");
		}
	}	
}
