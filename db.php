<?php
class db
{
	private $hostname = "virginia-inf-reporting01.inferencecommunications.com";
	private $dbname = "inference_demo_studio_instance_ac1451";
	private $tablename = "log_custom_Ivr_Dir";
	private $username = "slaveroot";
	private	$password = "Bradsgoat";
	private $dbh;

	public function __construct()
	{
		$this->dbh = mysqli_connect($this->hostname, $this->username, $this->password, $this->dbname);
		if (!$this->dbh) {
			die("Connection failed: " . mysqli_connect_error());
		}
	}

	public function getRow($dnis)
	{
		$sql = sprintf('SELECT * FROM %s WHERE DNIS = "%s"', $this->tablename, $dnis);
		$result = mysqli_query($this->dbh, $sql);
		if ($result) {
			return mysqli_fetch_assoc($result);
		} else {
			return FALSE;
		}
	}
}
