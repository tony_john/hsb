<?php
require_once getcwd() . DIRECTORY_SEPARATOR . "lib" . DIRECTORY_SEPARATOR . "nusoap.php";

$endpoint = "https://faxws.us.retarus.com/Faxolution201203/getListOfAvailableFaxReports";
$client = new nusoap_client("https://faxws.us.retarus.com/Faxolution201203?wsdl", true);

$error = $client->getError();
if ($error) {
    echo "<h2>Constructor error</h2><pre>" . $error . "</pre>";
}

//$wsdl = new wsdl("https://faxws.us.retarus.com/Faxolution201203?wsdl");
//$types = $wsdl->getTypeDef("userPassword", "http://retarus.com/fax4ba/faxws/2012/03");
//var_dump($types);exit();

/*$msg = $client->serializeEnvelope('
<ns2:AvailableReportsRequest
xmlns:ns2="http://retarus.com/fax4ba/faxws/2012/03">
        <username>inference-hsb-ivr@inferencesolutions.com</username>
        <password>!Password02</password>
</ns2:AvailableReportsRequest>
');

$result = $client->send($msg, $endpoint);*/

//$userPassword = array('username' => 'inference-hsb-ivr@inferencesolutions.com', 'password' => '!Password02');
//$result = $client->call('getListOfAvailableFaxReports', array("AvailableReportsRequest" => $userPassword));
//$doc = file_get_contents('https://www.hsbdirectoriesonline.com/bensum/plandocs/CBUSAMED61.pdf');
//$doc64 = base64_encode('https://www.hsbdirectoriesonline.com/bensum/plandocs/CBUSAMED61.pdf');
//$doc64 = base64_encode($doc);

//$doc = array("ToFaxNum!" => "9175516559",
//             "TaxId!" => "89-8978675",
//             "CurrentDate!" => "10/15/2015",
//             "AltId!" => "A00770294",
//             "FirstName!" => "LINDA",
//             "LastName!" => "HOWELL",
//             "GroupId!" => "LVOAK",
//             "StartDate!" => "10/12/2012",
//             "EndDate!" => "10/12/2016");
//$doc = "<ToFaxNum>9175516559</ToFaxNum>
//	<TaxId>89-8978675</TaxId>
//	<CurrentDate>10/15/2015</CurrentDate>
//	<AltId>A00770294</AltId>
//	<FirstName>LINDA</FirstName>
//	<LastName>HOWELL</LastName>
//	<GroupId>LVOAK</GroupId>
//	<StartDate>10/12/2012</StartDate>
//	<EndDate>10/12/2016</EndDate>";
//$doc64 = base64_encode($doc);

$altId = array('key' => 'AltId', 'value' => 'A00770294');
$faxNum = array('key' => 'ToFaxNum', 'value' => 'A00770294');
$tax = array('key' => 'TaxId', 'value' => 'A00770294');
$date = array('key' => 'CurrentDate', 'value' => 'A00770294');
$first = array('key' => 'FirstName', 'value' => 'A00770294');
$last = array('key' => 'LastName', 'value' => 'A00770294');
$group = array('key' => 'GroupId', 'value' => 'A00770294');
$start = array('key' => 'StartDate', 'value' => 'A00770294');
$end = array('key' => 'EndDate', 'value' => 'A00770294');

$faxRecipient = array('number' => '9175516559');
//$faxRecipient[] = new soapval('number', 'XSD_STRING', '9175516559');
$faxRecipient[] = new soapval('property', 'recipientProperty', $altId);
$faxRecipient[] = new soapval('property', 'recipientProperty', $faxNum);
$faxRecipient[] = new soapval('property', 'recipientProperty', $tax);
$faxRecipient[] = new soapval('property', 'recipientProperty', $date);
$faxRecipient[] = new soapval('property', 'recipientProperty', $first);
$faxRecipient[] = new soapval('property', 'recipientProperty', $last);
$faxRecipient[] = new soapval('property', 'recipientProperty', $group);
$faxRecipient[] = new soapval('property', 'recipientProperty', $start);
$faxRecipient[] = new soapval('property', 'recipientProperty', $end);

$property = array();
$property[] = array('key' => 'AltId', 'value' => 'blah');
$property[] = array('key' => 'ToFaxNum', 'value' => 'klah');
$property[] = array('key' => 'TaxId', 'value' => 'A007704');
$property[] = array('key' => 'CurrentDate', 'value' => 'asdf00770294');
$property[] = array('key' => 'FirstName', 'value' => 'A00770asdf294');
$property[] = array('key' => 'LastName', 'value' => 'A0077029ppppp4');
$property[] = array('key' => 'GroupId', 'value' => 'A00770294');
$property[] = array('key' => 'StartDate', 'value' => 'A00770294');
$property[] = array('key' => 'EndDate', 'value' => 'A00770294');
$data = array(
		'username' => 'inference-hsb-ivr@inferencesolutions.com',
		'password' => '!Password02',
		//'faxRecipient' => array('number' => '9175516559', 'property' => array('key' => 'AltId', 'value' => 'A00770294'),
		//									   'property' => array('key' => 'ToFaxNum', 'value' => 'A00770294'),
		//									    'property' => array('key' => 'TaxId', 'value' => 'A00770294'),
		//									    'property' => array('key' => 'CurrentDate', 'value' => 'A00770294'),
		//									    'property' => array('key' => 'FirstName', 'value' => 'A00770294'),
		//									    'property' => array('key' => 'LastName', 'value' => 'A00770294'),
		//								 	    'property' => array('key' => 'GroupId', 'value' => 'A00770294'),
		//									    'property' => array('key' => 'StartDate', 'value' => 'A00770294'),
		//									    'property' => array('key' => 'EndDate', 'value' => 'A00770294')),
		'faxRecipient' => array('number' => '9175516559', 'property' => $property),
//		'faxRecipient' => new soapval('faxRecipient', 'faxRecipient', $faxRecipient),
//		'document' => array('name' => 'document.txt', 'data' => 'SGVsbG8gd29ybGQh') 
		'document' => array('name' => 'doc00.html'),
		'options' => array('coverpageTemplateName' => 'HSBBenefitsFaxback.html')
	);
//$faxRecipient = "<number>9175516559</number><property><key>AltId</key><value>A00770294</value></property><property><key>ToFaxNum</key><value>A00770294</value></property><property><key>TaxId</key><value>A00770294</value></property><property><key>CurrentDate</key><value>A00770294</value></property><property><key>FirstName</key><value>A00770294</value></property><property><key>LastName</key><value>A00770294</value></property><property><key>GroupId</key><value>A00770294</value></property><property><key>StartDate</key><value>A00770294</value></property><property><key>EndDate</key><value>A00770294</value></property>";
//$data['faxRecipient'] = $faxRecipient;

$result = $client->call('sendFaxJob', array('JobRequest' => $data));


if ($client->fault) {
    echo "<h2>Fault</h2><pre>";
    print_r($result);
    echo "</pre>";
}
else {
    $error = $client->getError();
    if ($error) {
        echo "<h2>Error</h2><pre>";
        print_r($error);
        echo "</pre>";
    }
    else {
        echo "<h2>Result</h2><pre>";
        print_r($result);
        echo "</pre>";
    }
}

// show soap request and response
echo "<h2>Request</h2>";
echo "<pre>" . htmlspecialchars($client->request, ENT_QUOTES) . "</pre>";
echo "<h2>Response</h2>";
echo "<pre>" . htmlspecialchars($client->response, ENT_QUOTES) . "</pre>";
