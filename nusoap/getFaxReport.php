<?php
require_once getcwd() . DIRECTORY_SEPARATOR . "lib" . DIRECTORY_SEPARATOR . "nusoap.php";

$endpoint = "https://faxws.us.retarus.com/Faxolution201203/getListOfAvailableFaxReports";
$client = new nusoap_client("https://faxws.us.retarus.com/Faxolution201203?wsdl", true);

$error = $client->getError();
if ($error) {
    echo "<h2>Constructor error</h2><pre>" . $error . "</pre>";
}

//$wsdl = new wsdl("https://faxws.us.retarus.com/Faxolution201203?wsdl");
//$types = $wsdl->getTypeDef("userPassword", "http://retarus.com/fax4ba/faxws/2012/03");
//var_dump($types);exit();

/*$msg = $client->serializeEnvelope('
<ns2:AvailableReportsRequest
xmlns:ns2="http://retarus.com/fax4ba/faxws/2012/03">
        <username>inference-hsb-ivr@inferencesolutions.com</username>
        <password>!Password02</password>
</ns2:AvailableReportsRequest>
');

$result = $client->send($msg, $endpoint);*/

//$userPassword = array('username' => 'inference-hsb-ivr@inferencesolutions.com', 'password' => '!Password02');
//$result = $client->call('getListOfAvailableFaxReports', array("AvailableReportsRequest" => $userPassword));
$data = array(
                'username' => 'inference-hsb-ivr@inferencesolutions.com',
                'password' => '!Password02',
        	'jobId' => 'FJIHNDVV0Z0JPSJ24OJKLV'      
	 );
$result = $client->call('getFaxReport', array('ReportRequest' => $data));


if ($client->fault) {
    echo "<h2>Fault</h2><pre>";
    print_r($result);
    echo "</pre>";
}
else {
    $error = $client->getError();
    if ($error) {
        echo "<h2>Error</h2><pre>";
        print_r($error);
        echo "</pre>";
    }
    else {
        echo "<h2>Result</h2><pre>";
        print_r($result);
        echo "</pre>";
    }
}

// show soap request and response
echo "<h2>Request</h2>";
echo "<pre>" . htmlspecialchars($client->request, ENT_QUOTES) . "</pre>";
echo "<h2>Response</h2>";
echo "<pre>" . htmlspecialchars($client->response, ENT_QUOTES) . "</pre>";
