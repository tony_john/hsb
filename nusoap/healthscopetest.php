<?php
require_once getcwd() . DIRECTORY_SEPARATOR . "lib" . DIRECTORY_SEPARATOR . "nusoap.php";

//$endpoint = "http://services.healthaxis.com/IVR/MedicalLookup";
$client = new nusoap_client("https://connect.healthaxis.com/ivr/IVR.asmx?WSDL", true);
$client->soap_defencoding = 'UTF-8';
//$client->soapaction = 'http://services.healthaxis.com/IVR/MedicalLookup';

$error = $client->getError();
if ($error) {
    echo "<h2>Constructor error</h2><pre>" . $error . "</pre>";
}

//$wsdl = new wsdl("https://connect.healthaxis.com/ivr/IVR.asmx?WSDL");
//$types = $wsdl->getTypeDef("MedicalLookup", "http://services.healthaxis.com/IVR");
//var_dump($types);exit();

//$msg = $client->serializeEnvelope('<MedicalLookup xmlns="http://services.healthaxis.com/IVR"><Account></Account><GroupId>LAFRA</GroupId><AltId>570A20439</AltId><DOB>12171968</DOB></MedicalLookup>');

//$result = $client->send($msg, $endpoint);

$data = array(		
		"Account" => $_REQUEST['Account'],
		"GroupId" => $_REQUEST['GroupId'],
		"AltId" => $_REQUEST['AltId'],
		"DOB" => $_REQUEST['DOB'],
		"TaxId" => $_REQUEST['TaxId'],
		"ServiceDate" => $_REQUEST['ServiceDate']
		//"ServiceDate" => ""
	);

$result = $client->call($_REQUEST['CoverageType'], $data);


if ($client->fault) {
    echo "<h2>Fault</h2><pre>";
    print_r($result);
    echo "</pre>";
}
else {
    $error = $client->getError();
    if ($error) {
        echo "<h2>Error</h2><pre>";
        print_r($error);
        echo "</pre>";
    }
    else {
        echo "<h2>Result</h2><pre>";
        print_r($result);
        echo "</pre>";
    }
}

// show soap request and response
echo "<h2>Request</h2>";
echo "<pre>" . htmlspecialchars($client->request, ENT_QUOTES) . "</pre>";
echo "<h2>Response</h2>";
echo "<pre>" . htmlspecialchars($client->response, ENT_QUOTES) . "</pre>";
