<?php

function getArrayKey($key, $array, $default = '')
{
    if (is_array($array) && (isset($array[$key]) || array_key_exists($key, $array))) {
        return $array[$key];
    }
    return $default;
}