<?php
//ini_set("log_errors", 1);
//error_reporting(E_ALL);
include "config.php";
include "library.php";

function autoload($classname)
{
	$filename = $classname . ".php";
        if (is_readable($filename)) {
        	include_once $filename;
        }              
}

function autoloadNusoap($classname) 
{
	$filename = getcwd() . DIRECTORY_SEPARATOR . "nusoap" . DIRECTORY_SEPARATOR . "lib" . DIRECTORY_SEPARATOR . "nusoap.php";
	if (is_readable($filename)) {
		include_once $filename;
	}
}

function getStatusCodeMessage($status)
{
    $codes = Array(
        100 => 'Continue',
        101 => 'Switching Protocols',
        200 => 'OK',
        201 => 'Created',
        202 => 'Accepted',
        203 => 'Non-Authoritative Information',
        204 => 'No Content',
        205 => 'Reset Content',
        206 => 'Partial Content',
        300 => 'Multiple Choices',
        301 => 'Moved Permanently',
        302 => 'Found',
        303 => 'See Other',
        304 => 'Not Modified',
        305 => 'Use Proxy',
        306 => '(Unused)',
        307 => 'Temporary Redirect',
        400 => 'Bad Request',
        401 => 'Unauthorized',
        402 => 'Payment Required',
        403 => 'Forbidden',
        404 => 'Not Found',
        405 => 'Method Not Allowed',
        406 => 'Not Acceptable',
        407 => 'Proxy Authentication Required',
        408 => 'Request Timeout',
        409 => 'Conflict',
        410 => 'Gone',
        411 => 'Length Required',
        412 => 'Precondition Failed',
        413 => 'Request Entity Too Large',
        414 => 'Request-URI Too Long',
        415 => 'Unsupported Media Type',
        416 => 'Requested Range Not Satisfiable',
        417 => 'Expectation Failed',
        500 => 'Internal Server Error',
        501 => 'Not Implemented',
        502 => 'Bad Gateway',
        503 => 'Service Unavailable',
        504 => 'Gateway Timeout',
        505 => 'HTTP Version Not Supported'
    );
 
    return (isset($codes[$status])) ? $codes[$status] : '';
}

function sendResponse($body, $status, $content_type = 'application/json')
{
	header("HTTP/1.1 " . $status . getStatusCodeMessage($status));
	header('Content-type: ' . $content_type);
	echo $body;
}

spl_autoload_register("autoload");
spl_autoload_register("autoloadNusoap");
$endPoint = $_GET['request'];
//$args = explode('/', rtrim($params, '/'));
//$endPoint = array_shift($args);
$uuid = isset($_REQUEST['uuid']) ? $_REQUEST['uuid'] : '';

$hsb = new hsb();
switch($endPoint) {
	case 'members': 
		$result = $hsb->maskMemberId($_REQUEST['member'], $_REQUEST['dnis']);
		break;
	case 'claims':
		$member = isset($_REQUEST['member']) ? $_REQUEST['member'] : '';
		$account = isset($_REQUEST['account']) ? $_REQUEST['account'] : '';
		$group = isset($_REQUEST['group']) ? $_REQUEST['group'] : '';
		$dob = isset($_REQUEST['dob']) ? $_REQUEST['dob'] : '';
		$tax = isset($_REQUEST['tax']) ? $_REQUEST['tax'] : '';
		$dos = isset($_REQUEST['dos']) ? $_REQUEST['dos'] : '';
		$dnis = isset($_REQUEST['dnis']) ? $_REQUEST['dnis'] : '';
		$claimNo = isset($_REQUEST['claimNo']) ? $_REQUEST['claimNo'] : 0;
		//isset($_REQUEST['claimNo']) ? $claimNo = $_REQUEST['claimNo'] : $claimNo = 0;
		$alphaChars = isset($_REQUEST['alphachars']) ? $_REQUEST['alphachars'] : '';
		$result = $hsb->getClaimInfo($member, $account, $group, $dob, $tax, $dos,  $dnis, $claimNo, $alphaChars);		
		break;
	case 'eligibility':
		$member = isset($_REQUEST['member']) ? $_REQUEST['member'] : '';
                $account = isset($_REQUEST['account']) ? $_REQUEST['account'] : '';
                $group = isset($_REQUEST['group']) ? $_REQUEST['group'] : '';
                $dob = isset($_REQUEST['dob']) ? $_REQUEST['dob'] : '';
		$coverage = isset($_REQUEST['coverage']) ? $_REQUEST['coverage'] : '';
                $dnis = isset($_REQUEST['dnis']) ? $_REQUEST['dnis'] : '';
		$alphaChars = isset($_REQUEST['alphachars']) ? $_REQUEST['alphachars'] : '';
		$result = $hsb->getEligibility($member, $account, $group, $dob, $coverage, $dnis, $alphaChars);
                break;
	case 'benefitsFax':
		$member = isset($_REQUEST['member']) ? $_REQUEST['member'] : '';
                $account = isset($_REQUEST['account']) ? $_REQUEST['account'] : '';
                $group = isset($_REQUEST['group']) ? $_REQUEST['group'] : '';
                $tax = isset($_REQUEST['tax']) ? $_REQUEST['tax'] : '';
                $dob = isset($_REQUEST['dob']) ? $_REQUEST['dob'] : '';
                $coverage = isset($_REQUEST['coverage']) ? $_REQUEST['coverage'] : '';
		$faxNo = isset($_REQUEST['faxNo']) ? $_REQUEST['faxNo'] : '';
		$summaryDoc = isset($_REQUEST['summaryDoc']) ? $_REQUEST['summaryDoc'] : '';
		$result = $hsb->benefitsFax($member, $account, $group, $tax, $dob, $coverage, $summaryDoc, $faxNo);
		break;
	case 'eligibilityFax':
                $member = isset($_REQUEST['member']) ? $_REQUEST['member'] : '';
                $account = isset($_REQUEST['account']) ? $_REQUEST['account'] : '';
                $group = isset($_REQUEST['group']) ? $_REQUEST['group'] : '';
                $tax = isset($_REQUEST['tax']) ? $_REQUEST['tax'] : '';
                $dob = isset($_REQUEST['dob']) ? $_REQUEST['dob'] : '';
                $coverage = isset($_REQUEST['coverage']) ? $_REQUEST['coverage'] : '';
		$faxNo = isset($_REQUEST['faxNo']) ? $_REQUEST['faxNo'] : '';	
                $result = $hsb->eligibilityFax($member, $account, $group, $tax, $dob, $coverage, $faxNo);
                break;
	case 'claimsFax':
                $member = isset($_REQUEST['member']) ? $_REQUEST['member'] : '';
                $account = isset($_REQUEST['account']) ? $_REQUEST['account'] : '';
                $group = isset($_REQUEST['group']) ? $_REQUEST['group'] : '';
                $dob = isset($_REQUEST['dob']) ? $_REQUEST['dob'] : '';
                $tax = isset($_REQUEST['tax']) ? $_REQUEST['tax'] : '';
		$faxNo = isset($_REQUEST['faxNo']) ? $_REQUEST['faxNo'] : '';
		$dos = isset($_REQUEST['dos']) ? $_REQUEST['dos'] : '';
                $result = $hsb->claimsFax($member, $account, $group, $dob, $tax, $dos, $faxNo);
                break;
	default: 
		$result = array('message' => "Method does not exist!!", 'status' => 404, 'type' => 'text/plain');
} 
//error_log("Result: " . implode(", ", $result) . " --- Time: " . date("Y-m-d H:i:s") . "\n", 3, "./my_error.log");
$result['endpoint'] = $endPoint;
	__log($result, $uuid);
//error_log("Hello", 3, "./my_error.log");
sendResponse($result['message'], $result['status'], $result['type']);
