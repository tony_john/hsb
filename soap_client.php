<?php
class soap_client
{
	private $client;
	private $status;
	private $error;
	private $result;

	public function __construct($wsdl)
	{
		if (!isset($wsdl)) {
			throw new exception("WSDL is needed!");
		}
		$this->client = new nusoap_client($wsdl, true);
		$this->client->soap_defencoding = 'UTF-8';
		$this->error = $this->client->getError();
		if ($this->error) {
			$this->status = "error";
			throw new exception("Construct error: " . $this->error);
		}
	}

	public function __get($name) 
	{
		$getArray = array("status", "error", "result");
		if (in_array($name, $getArray)) {
			return $this->$name;
		}
	}

	public function soapCall($endPoint, $data)
	{
		$this->result = $this->client->call($endPoint, $data);
		if ($this->client->fault) {
			$this->status = "fault";
		} else {
			$this->error = $this->client->getError();
	                if ($this->error) {
        	                $this->status = "error";
				throw new exception("Remote method call error: " . $this->error);
                	} else {
				$this->status = "ok";
			}
		}		
	}
}
